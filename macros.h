#pragma once

#define MAIN_SAFE_CALL(func)                                 \
    {                                                        \
        const char *errMsg = func;                           \
        if (errMsg != nullptr) {                             \
            std::cerr << " ERROR!!! " << errMsg << " !!!\n"; \
            return 0;                                        \
        }                                                    \
    }

#define INNER_SAFE_CALL(func)                     \
    {                                             \
        const char *errMsg = func;                \
        if (errMsg != nullptr) { return errMsg; } \
    }
