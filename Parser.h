#pragma once

#include <string>

class Parser {
public:
    Parser(const int argc, char *argv[]);
    Parser(const Parser &) = delete;
    Parser &operator=(const Parser &) = delete;
    const char *isGood() const;
    std::string getXyzFile() const;
    std::string getBasisFile() const;
    int getChare() const;

private:
    std::string xyzfile{""};
    std::string basisfile{""};
    int charge{0};
};
