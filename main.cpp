/* Глобальная последовательность действий, которую мы хотим сделать
 * 1. Разобрать параметры командной строки и получить входные данные о задаче
 * 2. Считать параметры электронного гамильтониана (базисную библиотеку, геометрию, заряд)
 * 3. Создать базисный набор для нашей задачи (он будет жить в объекте Молекула, которая,
 * строго говоря, синглтон)
 * 4. Расчитать матричные элементы всех операторов
 * 5. Выполнить стартовый расчет в приближении голых ядер
 * 6. Выполнить расчет методом Хартри-Фока
 *
 * Комментарии к реализации
 * 1. Все делает клаcc Parser
 * 2. Функция input  < - мы сейчас тут
 * 3. Функция preproccesing
 * 4. Функция calcInt
 * 5. Функция hCore
 * 6. Функция HF
 */

#include <iostream>
#include "macros.h"
#include "BasisLib.h"
#include "Molecule.h"
#include "Parser.h"

const char *input(const Parser &pars, Molecule &mol, BasisLib &basisLib) {
    INNER_SAFE_CALL(mol.loadFromXyz(pars.getXyzFile()));
    INNER_SAFE_CALL(basisLib.loadFromGamessUS(pars.getBasisFile()));
    return nullptr;
}

const char *preprocessing(const Parser &pars, Molecule &mol,
                          const BasisLib &basisLib) {
    return "Error here!";
    return nullptr;
}

const char *calcIntegrals(const Parser &pars) { return nullptr; }

const char *print_basis(BasisLib &basisLib) {
    //  INNER_SAFE_CALL(basisLib.saveGamessUS_basis("test_basis.txt"));
    return nullptr;
}

int main(int argc, char *argv[]) {
    Molecule mol;
    BasisLib basisLib;
    Parser pars(argc, argv);
    MAIN_SAFE_CALL(pars.isGood());
    MAIN_SAFE_CALL(input(pars, mol, basisLib));
    MAIN_SAFE_CALL(print_basis(basisLib));
#ifdef DEBUG_INFO
    std::cout << mol.atoms.size() << " atoms were read.\n";
    auto totalQ = 0;
    for (const auto &a : mol.atoms) totalQ += a.q;
    std::cout << "Total nuclear charge is equal: " << totalQ << std::endl;
#endif
    /*
    MAIN_SAFE_CALL(preprocessing(pars, mol, basisLib));
    MAIN_SAFE_CALL(calcIntegrals(pars));
*/
    return 0;
}
